The BPM-Matlab project has been moved to GitHub:
https://github.com/ankrh/BPM-Matlab

Comments and questions can be directed to Anders Kragh Hansen, ankrh@fotonik.dtu.dk